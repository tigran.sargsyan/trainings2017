#include <string>

class GradeBook
{
public:
    GradeBook(std::string nameOfCourse, std::string nameOfTeacher);
    void setCourseName(std::string nameOfCourse);     
    std::string getCourseName();
    void setCourseTeacher(std::string nameOfTeacher);
    std::string getCourseTeacher();
    void displayMessage();
private:
    std::string courseName_; 
    std::string courseTeacher_;
};     
