#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        double amount = 24.00;
        std::cout << "Rate: " << rate << "%" << std::endl;
        std::cout << "Year" << std::setw(24) << "Amount of deposit" << std::endl;
        std::cout << std::fixed << std::setprecision(2);
        double accumulation = 1 + static_cast<double>(rate) / 100;
        for (int year = 1; year <= 379; ++year) {
            amount = amount * accumulation;
            std::cout << std::setw(4) << year << std::setw(24) << amount << std::endl;
        }
        std::cout << std::endl;
    }
    return 0;
}

