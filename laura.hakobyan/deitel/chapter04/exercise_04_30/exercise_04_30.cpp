#include <iostream>
#include <iomanip>

int
main()
{
    double r;
    std::cout << "Type radius of a circle: ";
    std::cin >> r;
    if (r < 0) {
        std::cout << "The radius of a circle cannot be minus" << std::endl;
        return 1;
    }
    double pi = 3.14159; 
    std::cout << "Circle's diameter is " << std::fixed << std::setprecision(5) << r * 2 << std::endl;
    std::cout << "Circumference is " << std::fixed << std::setprecision(5) << 2 * pi * r << std::endl;
    std::cout << "Area is " << std::fixed << std::setprecision(5) << pi * r * r << std::endl;
    return 0;
}

