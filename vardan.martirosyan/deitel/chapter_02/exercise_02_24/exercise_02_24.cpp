#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter number: ";
    std::cin  >> number;

    if (0 == number) {
         std::cout << "Error 1: Division by zero." << std::endl;
         return 1;
    }
    if(0 == number % 2) {
        std::cout << number << " is even number." << std::endl;
        return 0;
    }
    std::cout << number << " is odd number." << std::endl;

    return 0;
}

