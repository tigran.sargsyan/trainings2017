#include<iostream>

int
main()
{
    int number1;
    int number2;

    std::cout << "Insert two different whole numbers: ";
    std::cin >> number1 >> number2;

    if (0 == number2) {
        std::cout << "Error 1: division by zero." << std::endl;
        return 1;
    }
    if (number1 % number2 == 0) {
        std::cout << "The first is a multiple of the second" << std::endl;
        return 0;
    }
    std::cout << "The first is not a multiple of the second" << std::endl;

    return 0;
}
