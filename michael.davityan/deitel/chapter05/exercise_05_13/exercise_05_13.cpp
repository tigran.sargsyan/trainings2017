#include <iostream>

int
main()
{
    for (int row = 1; row <= 5; ++row) {
        int barChartSize;
        std::cin >> barChartSize;

        if (barChartSize < 1) {
            std::cout << "Error 1: wrong size." << std::endl;
            return 1;
        }
        if (barChartSize > 30) {
            std::cout << "Error 1: wrong size." << std::endl;
            return 1;
        }
    
        for (int column = 1; column <= barChartSize; ++column) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }

    return 0;
}
