#include "bankAccount.hpp"
#include <iostream>

int
main()
{   
    Account account1(1000);	
    Account account2(200);

    int deposit1;
    std::cout << "Please enter the amount you wish to deposit into Account 1: ";   
    std::cin >> deposit1;
    account1.credit(deposit1);
    std::cout << "The balance in the Account 1 is " << account1.getBalance() << "\n" << std::endl;
    
    
    int withdraw1 = 0;
    std::cout << "Please enter the amount you wish to withdraw from Account 1: ";
    std::cin >> withdraw1;
    account1.debit(withdraw1);
    std::cout << "The balance in the Account 1 is " << account1.getBalance() << "\n" << std::endl;
    

    int deposit2 = 0;
    std::cout << "Please enter the amount you wish to deposit into Account 2: ";
    std::cin >> deposit2;
    account2.credit(deposit2);
    std::cout << "The balance in the Account 2 is " << account2.getBalance() << "\n" << std::endl;

    int withdraw2 = 0;
    std::cout << "Please enter the amount you wish to withdraw from Account 2: ";
    std::cin >> withdraw2;
    account2.debit(withdraw2);
    std::cout << "The balance in the Account 2 is " << account2.getBalance() << "\n" << std::endl;

    return 0; 
}

