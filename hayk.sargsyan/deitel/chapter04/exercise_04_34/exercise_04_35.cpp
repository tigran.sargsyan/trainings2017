#include <iostream>
#include <string>

int
main()
{
    std::string mode;
    std::cout << "Please choose mode (encode = encoding mode, decode = decoding mode): ";
    std::cin >> mode;

    if ("encode" == mode) {
        int number;

        std::cout << "Enter number for encoding: ";
        std::cin >> number;

        if (number < 1000) {
            std::cerr << "Error 1: Please enter four digits number." << std::endl;
            return 1;
        }
        if (number > 9999) {
            std::cerr << "Error 1: Please enter four digits number." << std::endl;
            return 1;
        }

        int digit1 = (number / 1000 + 7) % 10;
        int digit2 = (number / 100 + 7) % 10;
        int digit3 = (number / 10 + 7) % 10;
        int digit4 = (number / 1 + 7) % 10;

        std::cout << "Encoded number: " << digit3 << digit4 << digit1 << digit2 << std::endl;
        
        return 0;
    }

    if ("decode" == mode) {
        int number;

        std::cout << "Enter number for encoding: ";
        std::cin >> number;

        if (number < 0) {
            std::cerr << "Error 1: The number must be four digits." << std::endl;
            return 1;
        }
        if (number > 9999) {
            std::cerr << "Error 1: The number must be four digits." << std::endl;
            return 1;
        }

        int digit1 = (number / 1000 + 3) % 10;
        int digit2 = (number / 100 + 3) % 10;
        int digit3 = (number / 10 + 3) % 10;
        int digit4 = (number / 1 + 3) % 10;    

        std::cout << "Decoded number: " << digit3 << digit4 << digit1 << digit2 << std::endl;
        
        return 0;
    }

    std::cerr << "Error 2: Please enter encode or decode." << std::endl;
    
    return 2;
}

