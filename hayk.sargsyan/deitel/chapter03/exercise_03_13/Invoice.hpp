#include <string>

class Invoice 
{
public:
    Invoice(std::string number, std::string about, int quantity, int price);
    void setNumber(std::string number);
    std::string getNumber();
    void setAbout(std::string about);
    std::string getAbout();
    void setQuantity(int quantity);
    int getQuantity();
    void setPrice(int price);
    int getPrice();
    int getInvoiceAmount();

private:
    std::string number_;
    std::string about_;
    int quantity_;
    int price_;
};

