#include <iostream>
#include "Account.hpp"

int
main()
{
    int balance1 = 500;
    Account account1(balance1);
    int credit1 = 700;
    account1.credit(credit1);
    int debit1 = 1000;
    account1.debit(debit1);
    int returnedBalance1 = account1.getBalance();
    std::cout << "Account 1 balance: " << returnedBalance1 << std::endl;
   
    std::cout << std::endl;

    int balance2 = 1000;
    Account account2(balance2);
    int credit2 = 100;
    account2.credit(credit2);
    int debit2 = 7000;
    account2.debit(debit2);
    int returnedBalance2 = account2.getBalance();
    std::cout << "Account 2 balance: " << returnedBalance2 << std::endl;

    return 0;
}
