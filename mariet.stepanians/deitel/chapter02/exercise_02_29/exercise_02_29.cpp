#include <iostream>

int
main ()
{
    std::cout << "integer  square  cube\n";
    std::cout << 0  << "        " << 0 * 0   << "       " << 0 * 0 * 0    << std::endl;
    std::cout << 1  << "        " << 1 * 1   << "       " << 1 * 1 * 1    << std::endl;
    std::cout << 2  << "        " << 2 * 2   << "       " << 2 * 2 * 2    << std::endl;
    std::cout << 3  << "        " << 3 * 3   << "       " << 3 * 3 * 3    << std::endl;
    std::cout << 4  << "        " << 4 * 4   << "      "  << 4 * 4 * 4    << std::endl;
    std::cout << 5  << "        " << 5 * 5   << "      "  << 5 * 5 * 5    << std::endl;
    std::cout << 6  << "        " << 6 * 6   << "      "  << 6 * 6 * 6    << std::endl;
    std::cout << 7  << "        " << 7 * 7   << "      "  << 7 * 7 * 7    << std::endl;
    std::cout << 8  << "        " << 8 * 8   << "      "  << 8 * 8 * 8    << std::endl;
    std::cout << 9  << "        " << 9 * 9   << "      "  << 9 * 9 * 9    << std::endl;
    std::cout << 10 << "       "  << 10 * 10 << "     "   << 10 * 10 * 10 << std::endl;
    
    return 0;
}
