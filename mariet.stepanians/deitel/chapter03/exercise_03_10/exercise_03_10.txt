A data member is usually declared private in a class so that only the member functions of the class in which the data member is declared can manipulate the variable. A class typically provides a set function and a get function for a data member to allow clients of the class to manipulate the data member in a controlled manner.
A set function should validate the data it is setting to ensure that invalid data is not placed in the object.
A get function can return the value of a data member without allowing clients to interact with the data member directly.
